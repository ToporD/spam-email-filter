import os
import math
import random

alpha = 0.1
eps = 0.00000001
stopWords = ['!', '?', '.', ',', ';', ':', '|', '\\', '/', '(', '(', '*', '&', '@', '#', '$', '%', '^', '`', '~', '[', ']', '{', '}', '|', '"', "'", '<', '>', '-', '_', '=', '+', 'subject:']

sslHam = []
sslSpam = []

def readStopWords(txt):
    file = open(txt, 'r')
    words = file.read().split()
    # words = [word.lower() for word in words]
    stopWords.append(words)
    file.close()

def loadFiles(filenames):
    f = open(filenames, 'r', encoding='latin-1')
    files = f.read().split()

    fileList = []

    for email in files:
        if 'spam' in email:
            email = 'enron6/spam/' + email
            fileList.append(email)
        elif 'ham' in email:
            email = 'enron6/ham/' + email
            fileList.append(email)
        else:
            email = 'ssl/' + email
            fileList.append(email)
            
    f.close()

    return fileList

def processEmail(files, spamDict, hamDict, joinedDict):
    uniqWords = 0
    nrOfHamEmails = 0
    nrOfSpamEmails = 0

    for emailName in files:
        f = open(emailName, 'r', encoding='latin-1')
        text = f.read().split()
        text = [word.lower() for word in text]

        if 'spam' in emailName or emailName in sslSpam:
            nrOfSpamEmails += 1
        elif 'ham' in emailName or emailName in sslHam:
            nrOfHamEmails += 1

        # print(emailName, emailName in sslHam, emailName in sslSpam)

        for word in text:
            if word not in stopWords:
                
                if word not in spamDict and word not in hamDict:
                    uniqWords += 1

                if 'spam' in emailName or emailName in sslSpam:
                    if word not in spamDict:
                        spamDict[word] = 1
                    else:
                        spamDict[word] += 1
                elif 'ham' in emailName or emailName in sslHam:
                    if word not in hamDict:
                        hamDict[word] = 1
                    else:
                        hamDict[word] += 1

                if word not in joinedDict:
                    joinedDict[word] = 1

        f.close()

    return nrOfSpamEmails, nrOfHamEmails, uniqWords

def trainAI(filenames, spamDict, hamDict):
    hamDict.clear()
    spamDict.clear()

    files = loadFiles(filenames)

    joinedDict = {}
    nrOfSpamEmails, nrOfHamEmails, uniqWords = processEmail(files, spamDict, hamDict, joinedDict)

    nrOfEmails = nrOfHamEmails + nrOfSpamEmails
    # print(nrOfHamEmails, nrOfSpamEmails, nrOfHamEmails + nrOfSpamEmails)
    # print(spamDict)

    probOfHam = nrOfHamEmails / nrOfEmails
    probOfSpam = nrOfSpamEmails / nrOfEmails

    return probOfSpam, probOfHam, uniqWords

def additiveFormula(word, hamDict, spamDict, uniqWords, nrOfSpamWords, nrOfHamWords):
    ham = 0
    spam = 0

    if word not in spamDict:
        spam = eps
    else:
        spam = (spamDict[word] + alpha) / (alpha * uniqWords + nrOfSpamWords)

    if word not in hamDict:
        ham = eps
    else:
        ham = (hamDict[word] + alpha) / (alpha * uniqWords + nrOfHamWords)

    return [spam, ham, 1]

def evaluate(filteredWords, probOfSpam, probOfHam):
    documentValue = 0
    for key in filteredWords:
        documentValue += filteredWords[key][2] * (math.log(filteredWords[key][0])  - math.log(filteredWords[key][1]))

    ln_r = math.log(probOfSpam) - math.log(probOfHam) + documentValue
    
    # if(ln_r > 0):
        # return 2

    # r = math.exp(ln_r) 
    return ln_r

def filterEmails(txt, hamDict, spamDict, probOfSpam, probOfHam, uniqWords):
    f = open(txt, 'r', encoding='latin-1')
    files = f.read().split()
    f.close()

    falseNegativ = 0
    falsePozitiv = 0
    failedDecision = 0

    nrOfHamEmails = 0
    nrOfSpamEmails = 0

    for email in files:
        path = ''
        wordInEamil = {}

        if 'spam' in email:
            path = 'enron6/spam/'
            nrOfSpamEmails += 1
        elif 'ham' in email:
            path = 'enron6/ham/'
            nrOfHamEmails += 1
        else:
            path = 'ssl/'
            if email in sslHam:
                nrOfSpamEmails += 1
            else:
                nrOfHamEmails += 1

        
        f = open(path + email, 'r', encoding='latin-1')
        text = f.read().split()
        text = [word.lower() for word in text]
        
        for word in text:
            if word not in stopWords:    
                if word not in wordInEamil:
                    wordInEamil[word] = additiveFormula(word, hamDict, spamDict, uniqWords, len(spamDict), len(hamDict))
                else:
                    wordInEamil[word][2] += 1

        ln_r = evaluate(wordInEamil, probOfSpam, probOfHam)
        if (ln_r > 0):
            if 'ham' in email or email in sslHam:
                falseNegativ += 1
                failedDecision += 1
        else:
            if 'spam' in email or email in sslSpam:
                falsePozitiv += 1
                failedDecision += 1

    return falseNegativ, falsePozitiv, falsePozitiv + falseNegativ, nrOfHamEmails, nrOfSpamEmails

def printOutResults(emailType, failedDecision, nrOfTestEmails, falseNegativ, nrOfTestSpamEmails, falsePozitiv, nrOfTestHamEmails):
    print("Data trained with alpha =", alpha)
    print("For " + emailType + " data: missed emails:", failedDecision, " from:", nrOfTestEmails, "it is:", (failedDecision * 100) / nrOfTestEmails, "%")
    print('False positive:', falsePozitiv, 'out of:', nrOfTestHamEmails, 'it is:', falsePozitiv * 100 / nrOfTestHamEmails, "%")
    print('False negative:', falseNegativ, 'out of:', nrOfTestSpamEmails, 'it is:', falseNegativ * 100 / nrOfTestSpamEmails, "%")
    print('----------------------------------------------------------------------\n')

def crossValidation(k, emails):
    lowerLimit = 0
    nrOfElementsInOne = len(emails) // k
    upperLimit = nrOfElementsInOne


    value = []

    for i in range(k):
        f = open('1outk.txt', 'a')
        oneEmailChunk = emails[lowerLimit : lowerLimit + upperLimit]
        for email in oneEmailChunk:
            f.write(email + "\n")
        f.close()
        

        otherEmailChunk = []
        f = open('k-1outk.txt', 'a')
        for email in emails:
            if email not in oneEmailChunk:
                f.write(email + "\n")
        f.close()

        hamDict = {}
        spamDict = {}

        probOfSpam, probOfHam, uniqWords = trainAI('k-1outk.txt', spamDict, hamDict)
        falseNegativ, falsePozitiv, missed, nrOfHamEmails, nrOfSpamEmails = filterEmails('1outk.txt', hamDict, spamDict, probOfSpam, probOfHam, uniqWords)

        value.append((missed * 100) / (nrOfHamEmails + nrOfSpamEmails))

        os.remove('1outk.txt')
        os.remove('k-1outk.txt')

        lowerLimit += nrOfElementsInOne
        upperLimit += nrOfElementsInOne

    return sum(value) / k

def alphaChanger():
    global alpha


    alphaValues = [0.00005, 0.0001, 0.0005, 0.001, 0.005, 0.01, 0.05, 0.1, 0.5, 1]

    bestAlphaValue = 0
    bestAlphaResult = 5

    f = open('train.txt', 'r')
    emails = f.read().split()
    f.close()

    for value in alphaValues:
        random.shuffle(emails)

        alpha = value
        
        result = crossValidation(5, emails)

        print("alpha:", alpha, "result:", result)

        if(result < bestAlphaResult):
            bestAlphaValue = alpha
            bestAlphaResult = result


    print('best alpha is:', bestAlphaValue, 'with value:', bestAlphaResult)

    alpha = 0.1

def getProbsOfEmail(email, hamDict, spamDict, probOfSpam, probOfHam, uniqWords):
    wordInEamil = {}

    falseNegativ = 0
    falsePozitiv = 0
    failedDecision = 0

    nrOfHamEmails = 0
    nrOfSpamEmails = 0

    f = open(email, 'r', encoding='latin-1')
    text = f.read().split()
    text = [word.lower() for word in text]
    
    for word in text:
        if word not in stopWords:    
            if word not in wordInEamil:
                wordInEamil[word] = additiveFormula(word, hamDict, spamDict, uniqWords, len(spamDict), len(hamDict))
            else:
                wordInEamil[word][2] += 1

    ln_r = evaluate(wordInEamil, probOfSpam, probOfHam)

    if ln_r > 700:
        R = math.exp(700)
    elif ln_r < -700:
        R = math.exp(-700)
    else:
        R = math.exp(ln_r)

    emailHam = 1 / (R + 1)
    emailSpam = R / (R + 1)

    return emailSpam, emailHam

def halfSupervised(hamDict, spamDict, probOfSpam, probOfHam, uniqWords):
    while True:
        changes = 0
        for filename in os.listdir("ssl"):
            emailSpam, emailHam = getProbsOfEmail("ssl/" + filename, hamDict, spamDict, probOfSpam, probOfHam, uniqWords)

            if emailSpam > emailHam and (('ssl/' + filename) not in sslHam and ('ssl/' + filename) not in sslSpam):
                # print(emailSpam / emailHam)
                if emailSpam / emailHam >= 12:
                    f = open('train.txt', 'a')
                    f.write('\n' + filename)
                    sslSpam.append('ssl/' + filename)
                    f.close()
                    changes += 1

            elif emailHam > emailSpam and (('ssl/' + filename) not in sslHam and ('ssl/' + filename) not in sslSpam):
                # print(emailHam / emailSpam)
                if emailHam / emailSpam >= 12:
                    f = open('train.txt', 'a')
                    f.write('\n' + filename)
                    sslHam.append('ssl/' + filename)
                    f.close()
                    changes += 1
        
        print(changes)

        if changes > 1:
            probOfSpam, probOfHam, uniqWords = trainAI('train.txt', spamDict, hamDict)
        else:
            break

    return probOfSpam, probOfHam, uniqWords

def main():
    hamDict = {}
    spamDict = {}

    readStopWords('stopwords2.txt')
    probOfSpam, probOfHam, uniqWords = trainAI('train.txt', spamDict, hamDict)
    
    falseNegativ, falsePozitiv, missed, nrOfHamEmails, nrOfSpamEmails = filterEmails('train.txt', hamDict, spamDict, probOfSpam, probOfHam, uniqWords)
    printOutResults('train', missed, nrOfHamEmails + nrOfSpamEmails, falseNegativ, nrOfSpamEmails, falsePozitiv, nrOfHamEmails)    


    falseNegativ, falsePozitiv, missed, nrOfHamEmails, nrOfSpamEmails = filterEmails('test.txt', hamDict, spamDict, probOfSpam, probOfHam, uniqWords)
    printOutResults('test', missed, nrOfHamEmails + nrOfSpamEmails, falseNegativ, nrOfSpamEmails, falsePozitiv, nrOfHamEmails)    

    # alphaChanger()    

    falseNegativ, falsePozitiv, missed, nrOfHamEmails, nrOfSpamEmails = filterEmails('train.txt', hamDict, spamDict, probOfSpam, probOfHam, uniqWords)
    printOutResults('train', missed, nrOfHamEmails + nrOfSpamEmails, falseNegativ, nrOfSpamEmails, falsePozitiv, nrOfHamEmails)    

    falseNegativ, falsePozitiv, missed, nrOfHamEmails, nrOfSpamEmails = filterEmails('test.txt', hamDict, spamDict, probOfSpam, probOfHam, uniqWords)
    printOutResults('test', missed, nrOfHamEmails + nrOfSpamEmails, falseNegativ, nrOfSpamEmails, falsePozitiv, nrOfHamEmails)

    probOfSpam, probOfHam, uniqWords = halfSupervised(hamDict, spamDict, probOfSpam, probOfHam, uniqWords)

    falseNegativ, falsePozitiv, missed, nrOfHamEmails, nrOfSpamEmails = filterEmails('train.txt', hamDict, spamDict, probOfSpam, probOfHam, uniqWords)
    printOutResults('train', missed, nrOfHamEmails + nrOfSpamEmails, falseNegativ, nrOfSpamEmails, falsePozitiv, nrOfHamEmails)    

    falseNegativ, falsePozitiv, missed, nrOfHamEmails, nrOfSpamEmails = filterEmails('test.txt', hamDict, spamDict, probOfSpam, probOfHam, uniqWords)
    printOutResults('test', missed, nrOfHamEmails + nrOfSpamEmails, falseNegativ, nrOfSpamEmails, falsePozitiv, nrOfHamEmails)


    os.system('./removeEmails.sh')

if __name__ =="__main__":
    main()