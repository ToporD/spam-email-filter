# Topor Daniel Balint tdim2093
import os
import math
import random

eps = 0.00000001
kk = 0
alpha = 0
uniqWords = 0
probOfHam = 0
probOfSpam = 0
nrOfEmails = 0
falseNegativ = 0
falsePozitiv = 0
nrOfHamWords = 0
nrOfSpamWords = 0
nrOfHamEmails = 0
nrOfSpamEmails = 0
failedDecision = 0
nrOfTestEmails = 0
nrOfTestHamEmails = 0
nrOfTestSpamEmails = 0

lowerLimit = 0
upperLimit = 0

crossOneDict = {}
crossAnyElseDict = {}

hamDict = {}
spamDict = {}
filteredWords = {}

stopWords = ['!', '?', '.', ',', ';', ':', '|', '\\', '/', '(', '(', '*', '&', '@', '#', '$', '%', '^', '`', '~', '[', ']', '{', '}', '|', '"', "'", '<', '>', '-', '_', '=', '+', 'subject:']

def resetVariables():
    global falseNegativ
    global falsePozitiv
    global failedDecision
    global nrOfTestEmails
    global nrOfTestHamEmails
    global nrOfTestSpamEmails

    falseNegativ = 0
    falsePozitiv = 0
    failedDecision = 0
    nrOfTestEmails = 0
    nrOfTestHamEmails = 0
    nrOfTestSpamEmails = 0

def readStopWords(filename):
    with open(filename, "r") as f:
        text = f.read()
        text = text.split()
        for word in text:
            stopWords.append(word)

def insertToDict(path, filename, dictionary):
    global uniqWords

    with open(os.path.join(path, filename), 'r', encoding='latin-1') as Email:
        words = Email.read()
        words = words.split()
        words = [word.lower() for word in words]
        for word in words:
            if word not in stopWords:
                if (word not in spamDict) and (word not in hamDict):
                    uniqWords += 1

                if word not in dictionary:
                    dictionary[word] = 1
                else:
                    dictionary[word] += 1
            

def choice(filename, formula):
    global nrOfHamEmails
    global nrOfSpamEmails

    if 'spam' in filename:
        nrOfSpamEmails += 1
        insertToDict("enron6/spam", filename, spamDict)
    else:
        nrOfHamEmails += 1
        insertToDict("enron6/ham", filename, hamDict)

def readEmails(txt, func, formula):
    with open(txt, 'r') as f:
        filenames = f.read()
        filenames = filenames.split()
        for filename in filenames:
            func(filename, formula)

def partionating(filename, formula):
    global kk
    global lowerLimit
    global upperLimit

    if kk in range(lowerLimit, upperLimit):
        file = open("oneSection.txt", "a")
        file.write(filename + "\n")
        file.close()
    else:
        file = open("moreSection.txt", "a")
        file.write(filename + "\n")
        file.close()
    kk += 1

def additiveFormula(word):
    ham = 0
    spam = 0

    if word not in spamDict:
        spam = eps
    else:
        spam = (spamDict[word] + alpha) / (alpha * uniqWords + nrOfSpamWords)

    if word not in hamDict:
        ham = eps
    else:
        ham = (hamDict[word] + alpha) / (alpha * uniqWords + nrOfHamWords)

    return [spam, ham, 1]

def simpleFormula(word):
    ham = 0
    spam = 0
    
    if word not in spamDict:
        spam = eps
    else:   
        spam = spamDict[word] / nrOfSpamWords

    if word not in hamDict:
        ham = eps
    else:
        ham = hamDict[word] / nrOfHamWords

    return [spam, ham, 1]

def evaluate():
    documentValue = 0
    for key in filteredWords:
        documentValue += filteredWords[key][2] * (math.log(filteredWords[key][0])  - math.log(filteredWords[key][1]))

    ln_r = math.log(probOfSpam) - math.log(probOfHam) + documentValue
    
    if(ln_r > 0):
        return 2

    r = math.exp(ln_r) 
    return r

def wordInTest(filename, formula):
    global falseNegativ
    global falsePozitiv
    global failedDecision
    global nrOfTestEmails
    global nrOfTestHamEmails
    global nrOfTestSpamEmails

    nrOfTestEmails += 1

    filteredWords.clear()

    path = ""

    if('spam' in filename):
        nrOfTestSpamEmails += 1
        path = "enron6/spam"
    else:
        nrOfTestHamEmails += 1
        path = "enron6/ham"

    with open(os.path.join(path, filename), 'r', encoding='latin-1') as f:
        text = f.read()
        text = text.split()
        text = [word.lower() for word in text]
        for word in text:
            if word not in stopWords:
                if word not in filteredWords:
                    filteredWords[word] = formula(word)
                else:
                    filteredWords[word][2] += 1

        L = evaluate()
        if (L > 1):
            if 'ham' in filename:
                falseNegativ += 1
                failedDecision += 1
        else:
            if 'spam' in filename:
                falsePozitiv += 1
                failedDecision += 1

def printOutResults(emailType):
    print("Data trained with alpha =", alpha)
    print("For " + emailType + " data: missed emails:", failedDecision, " from:", nrOfTestEmails, "it is:", (failedDecision * 100) / nrOfTestEmails, "%")
    print('False positive:', falsePozitiv, 'out of:', nrOfTestHamEmails, 'it is:', falsePozitiv * 100 / nrOfTestHamEmails, "%")
    print('False negative:', falseNegativ, 'out of:', nrOfTestSpamEmails, 'it is:', falseNegativ * 100 / nrOfTestSpamEmails, "%")
    print('----------------------------------------------------------------------\n')

def filterEmails(txt, formula, emailType):
    resetVariables()
    readEmails(txt, wordInTest, formula)
    printOutResults(emailType)

def crossValidation(k):
    global kk
    global lowerLimit
    global upperLimit

    # osszuk fel k reszre es ertekeljuk ki azokat

    nrOfElementsInOne = nrOfEmails // k
    lowerLimit = 0
    upperLimit = nrOfElementsInOne


    # a fajlokat kell felkeverjem es akkor abbol a listabol fogok tudni kerni dolgokat
    
    for i in range(k):
        kk = 0
        readEmails('train.txt', partionating, simpleFormula)
        filterEmails('oneSection.txt', simpleFormula, 'oneSection')

        lowerLimit += nrOfElementsInOne
        upperLimit += nrOfElementsInOne

def main():
    global alpha
    global probOfHam
    global probOfSpam
    global nrOfEmails
    global nrOfHamWords
    global nrOfSpamWords
    global nrOfHamEmails
    global nrOfSpamEmails

    readStopWords("stopwords2.txt")
    readEmails("train.txt", choice, choice)

    nrOfEmails = nrOfHamEmails + nrOfSpamEmails
    probOfHam = nrOfHamEmails / nrOfEmails
    probOfSpam = nrOfSpamEmails / nrOfEmails

    nrOfHamWords = sum(hamDict.values())
    nrOfSpamWords = sum(spamDict.values())

    filterEmails('train.txt', simpleFormula, 'trained')
    filterEmails("test.txt", simpleFormula, 'test')

    alpha = 0.01
    for i in range(3):
        filterEmails('train.txt', additiveFormula, 'trained')
        filterEmails("test.txt", additiveFormula, 'test')
        alpha *= 10

    # print(uniqWords)

if __name__=="__main__":
    main()